import { Component, OnInit } from '@angular/core';
import { SessionsFileService, SessionItem } from './sessions-file.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Video Archive';
  activeSession = '';
  constructor(private sessionsFileService:SessionsFileService){}

  ngOnInit(){
    // manually setting the active session to build on load since it's the first tab
    this.setDefaultActiveSessionForTab('Build');
  }

  setActiveSession(activeSession){
    this.activeSession = activeSession;
  }

  setDefaultActiveSessionForTab(tabName) {
    this.sessionsFileService.sessions.subscribe(sessions => {
      let first = sessions.filter((sess)=>{
            let track = Object.assign({Title:""}, sess.Track); // avoid weird webpack issue
            return track.Title  == tabName
      })[0] || new SessionItem({});
      this.setActiveSession(first)
    });
  }

}
