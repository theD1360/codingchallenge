import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs/Rx'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import {List, Record} from 'immutable';

const SessionItemRecord = Record({
  Description : "",
  Id : 0,
  IsFeatured: false,
  LastModified: [],
  Mandatory: false,
  MetadataValues: [],
  Published: false,
  Room: {},
  SessionType: {},
  Speakers: [],
  TimeSlot: {},
  Title: "",
  Track: {
    Title: ""
  }
});

export class SessionItem extends SessionItemRecord {
  Description : string;
  Id : number;
  IsFeatured: boolean;
  LastModified: string;
  Mandatory: boolean;
  MetadataValues: Object[];
  Published: boolean;
  Room: Object;
  SessionType: Object;
  Speakers: Object[];
  TimeSlot: Object;
  Title: string;
  Track: Object;

  constructor(props) {
    super(props);
  }

}

@Injectable()
export class SessionsFileService {

  sessionJson = '/assets/sessions.json';
  private _sessions: BehaviorSubject<List<SessionItem>> = new BehaviorSubject(List([]));
  private _tabs: BehaviorSubject<List<any>> = new BehaviorSubject(List([]));
  private _excludedTabs = ['Activities', 'Training']; // bad design choice but sticking to it for the sake of expediency

  constructor(private _http: HttpClient) {
    this.loadData();
  }

  get sessions() {
      return this._sessions.asObservable();
  }

  get tabs() {
    return this._tabs.asObservable();
  }

  getFile(): Observable<any> {
    return this._http.get(this.sessionJson)
  }

  loadData(){
    this.getFile().subscribe( (f) =>{

        let s = f.Items.map(i => new SessionItem(i));
        let t = f.Items.map(i => i.Track.Title).filter((value, index, self) => {
          return self.indexOf(value) === index;
        }).filter((value) => {
          return !(this._excludedTabs.indexOf(value) > -1)
        });
        this._sessions.next(s);
        this._tabs.next(t);

      return this._sessions
    }, error => console.log('Could not load sessions JSON file.'))
  }

}
