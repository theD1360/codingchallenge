### Atlassian Web Developer Mini Project

#### What do I need to do?

The task is relatively straightforward: build a simple site using the provided screenshots and data.

The point of this exercise is to get a feel for how you approach HTML, CSS, and JavaScript.

The [Atlassian Design Guidelines](https://atlassian.design) will be helpful for any assets and color values you might need, and the site content can be found in the included JSON file.

Feel free to use any libraries or frameworks you're comfortable with.

In terms of time, our general guidance (and what we hope seems fair to you too) is that you shouldn't spend more than a few hours putting things together. If you're feeling crunched, it's better to provide a written description or outline of how you'd approach something than to push yourself to get things into code.

Good luck!

#### Basic requirements

- Use the JSON data for the majority of the content and use static text where it makes sense
- Each 'track' should have its own tab, and the tabs should be functional (a page reload is OK when switching tabs)
- The left side nav items are session titles; they should be different on each tab since there are different sessions for each track
- Clicking a left side nav item should cause the content to change without a page reload

#### Bonus points 

*Don't stress on this part. If you've already spent a decent amount of time on the basic requirements, then a brief explanation of how you might handle this is absolutely fine. We mean it!*

- Change the URL based on the current track and session combination -- URLs should load as expected when navigated to directly (loading the correct track and session)
- Make it responsive -- use your own judgment how to deal with the horizontal nav

#### Delivery

Two options:

1. Zip the project and send it to dcorcoran@atlassian.com, or
2. Create a [Bitbucket](https://bitbucket.org) repository and send the repository URL to dcorcoran@atlassian.com. You can either make it public or keep it private and grant read access to dcorcoran@atlassian.com.
