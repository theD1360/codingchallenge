# Atlassian

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.7.1.


## Installation

Install angular CLI if not already installed globaly

```
npm install -g @angular/cli
```

Install project dependencies. (`./node_modules/` directory is not included in zip)
```
npm install
```

Run Dev server for demonstration on `http://localhost:4200/`
```
npm start
```

